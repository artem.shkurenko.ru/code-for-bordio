import { makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  page: {
    margin: theme.spacing(0, 0.5),
  },
  current: {
    pointerEvents: "none",
  },
  dot: {
    margin: theme.spacing(0, 1),
  },
}));

export default useStyles;
