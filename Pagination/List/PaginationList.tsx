import * as React from "react";
import clsx from "clsx";
import { Typography } from "@x5-react-uikit/core";
import { ChevronLeft, ChevronRight } from "@x5-react-uikit/icons";

import IconButton from "ui/IconButton";

import { PaginationProps } from "../types";
import useStyles from "./styles";

const PaginationList: React.VFC<PaginationProps> = (props) => {
  const classes = useStyles();

  const lastPage = React.useMemo(() => {
    return Math.ceil(props.total / props.size) - 1;
  }, [props.page, props.total, props.size]);

  const isFirstPage = React.useMemo(() => {
    return props.page === 0;
  }, [props.page]);

  const isLastPage = React.useMemo(() => {
    return lastPage === props.page;
  }, [props.page, props.total, props.size]);

  const handlePrevPage = React.useCallback(() => {
    if (!props.onChangePage || isFirstPage) return;
    props.onChangePage(props.page - 1);
  }, [props.page, props.total, props.size, props.onChangePage]);

  const handleNextPage = React.useCallback(() => {
    if (!props.onChangePage || isLastPage) return;
    props.onChangePage(props.page + 1);
  }, [props.page, props.total, props.size, props.onChangePage]);

  const list = React.useMemo<number[]>(() => {
    const data = [];
    let startPage = props.page;
    if (startPage !== 0) startPage--;
    if (lastPage === props.page) startPage--;
    for (let i = startPage; i < startPage + 3 && i <= lastPage; i++) {
      data.push(i);
    }
    return data;
  }, [props.page, props.total, props.size]);

  const handleChangePage = React.useCallback(
    (value) => {
      if (!props.onChangePage) return;
      props.onChangePage(value);
    },
    [props.onChangePage]
  );

  return (
    <div className={classes.root}>
      <IconButton
        size={"small"}
        variant={"ghost"}
        onClick={handlePrevPage}
        disabled={isFirstPage}
      >
        <ChevronLeft />
      </IconButton>
      {props.page > 1 && (
        <>
          <IconButton
            className={classes.page}
            size={"extra-small"}
            variant={"ghost"}
            onClick={() => handleChangePage(0)}
          >
            <Typography variant={"p1compact"}>1</Typography>
          </IconButton>
          {props.page > 2 && (
            <Typography className={classes.dot} variant={"p1compact"}>
              …
            </Typography>
          )}
        </>
      )}
      {list.map((item) => {
        const isCurrent = item === props.page;
        return (
          <IconButton
            key={item}
            className={clsx(classes.page, { [classes.current]: isCurrent })}
            size={"extra-small"}
            variant={isCurrent ? "outlined" : "ghost"}
            onClick={() => handleChangePage(item)}
          >
            <Typography variant={"p1compact"}>{item + 1}</Typography>
          </IconButton>
        );
      })}
      {lastPage > props.page + 1 && (
        <>
          {lastPage > props.page + 2 && (
            <Typography className={classes.dot} variant={"p1compact"}>
              …
            </Typography>
          )}

          <IconButton
            className={classes.page}
            size={"extra-small"}
            variant={"ghost"}
            onClick={() => handleChangePage(lastPage)}
          >
            <Typography variant={"p1compact"}>{lastPage + 1}</Typography>
          </IconButton>
        </>
      )}
      <IconButton
        size={"small"}
        variant={"ghost"}
        onClick={handleNextPage}
        disabled={isLastPage}
      >
        <ChevronRight />
      </IconButton>
    </div>
  );
};

export default PaginationList;
