import * as React from "react";
import { Meta, Story } from "@storybook/react";

import Pagination, { PaginationView } from "../Pagination";

export default {
  title: "UI/Pagination",
  component: Pagination,
} as Meta;

const Template = (view: PaginationView): Story => () => {
  const [size, setSize] = React.useState<number>(50);
  const [page, setPage] = React.useState<number>(0);
  return (
    <Pagination
      view={view}
      size={size}
      onChangeSize={setSize}
      page={page}
      onChangePage={setPage}
      total={555}
    />
  );
};

export const Switcher: Story = Template(PaginationView.SWITCHER);
export const List: Story = Template(PaginationView.LIST);
