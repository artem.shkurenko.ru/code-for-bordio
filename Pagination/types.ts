export interface PaginationProps {
  view?: PaginationView;
  total: number | undefined;
  page: number;
  size: number;
  onChangePage?: (value: number) => void;
  onChangeSize?: (value: number) => void;
}

export enum PaginationView {
  SWITCHER = "SWITCHER",
  LIST = "LIST",
}
