import * as React from "react";

import { PaginationProps, PaginationView } from "./types";
import PaginationSwitcher from "./Switcher";
import PaginationList from "./List";

const Pagination: React.VFC<PaginationProps> = (props) => {
  const view = props.view || PaginationView.SWITCHER;
  switch (view) {
    case PaginationView.SWITCHER:
      return <PaginationSwitcher {...props} />;
    case PaginationView.LIST:
      return <PaginationList {...props} />;
    default:
      return null;
  }
};

export default Pagination;
