import { makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  size: {
    width: theme.spacing(22),
  },
  text: {
    whiteSpace: "nowrap",
    margin: theme.spacing(0, 5),
  },
}));

export default useStyles;
