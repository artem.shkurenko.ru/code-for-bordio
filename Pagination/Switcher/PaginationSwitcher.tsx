import * as React from "react";
import { Typography, Select, SelectOptionType } from "@x5-react-uikit/core";
import { ChevronLeft, ChevronRight } from "@x5-react-uikit/icons";

import IconButton from "ui/IconButton";

import { PaginationProps } from "../types";
import useStyles from "./styles";

const sizes: SelectOptionType[] = [
  { name: "25", value: 25 },
  { name: "50", value: 50 },
  { name: "100", value: 100 },
];

const PaginationSwitcher: React.VFC<PaginationProps> = (props) => {
  const classes = useStyles();

  const handleChangeSize = React.useCallback(
    (_event, value) => {
      if (props.onChangeSize) {
        props.onChangeSize(value);
      }
      if (props.onChangePage) {
        props.onChangePage(0);
      }
    },
    [props.onChangeSize]
  );

  const isFirstPage = React.useMemo(() => {
    return props.page === 0;
  }, [props.page]);

  const isLastPage = React.useMemo(() => {
    return Math.ceil(props.total / props.size) - 1 === props.page;
  }, [props.page, props.total, props.size]);

  const handlePrevPage = React.useCallback(() => {
    if (!props.onChangePage || isFirstPage) return;
    props.onChangePage(props.page - 1);
  }, [props.page, props.total, props.size, props.onChangePage]);

  const handleNextPage = React.useCallback(() => {
    if (!props.onChangePage || isLastPage) return;
    props.onChangePage(props.page + 1);
  }, [props.page, props.total, props.size, props.onChangePage]);

  const startCount = React.useMemo(() => {
    return props.size * props.page + 1;
  }, [props.size, props.page]);

  const endCount = React.useMemo(() => {
    return Math.min(props.size * (props.page + 1), props.total);
  }, [props.size, props.page, props.total]);

  return (
    <div className={classes.root}>
      <div className={classes.size}>
        <Select
          options={sizes}
          value={props.size}
          onChange={handleChangeSize}
          stretch={true}
          size={"small"}
        />
      </div>
      {props.total && (
        <>
          <Typography className={classes.text} variant={"p1compact"}>
            {startCount}–{endCount} из {props.total}
          </Typography>
          <IconButton
            size={"small"}
            variant={"ghost"}
            onClick={handlePrevPage}
            disabled={isFirstPage}
          >
            <ChevronLeft />
          </IconButton>
          <IconButton
            size={"small"}
            variant={"ghost"}
            onClick={handleNextPage}
            disabled={isLastPage}
          >
            <ChevronRight />
          </IconButton>
        </>
      )}
    </div>
  );
};

export default PaginationSwitcher;
