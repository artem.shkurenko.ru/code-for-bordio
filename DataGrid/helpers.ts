import * as React from "react";

export interface DataGridContextValue {
  containerEl: React.RefObject<HTMLDivElement | null>;
}

export const DataGridContext = React.createContext<DataGridContextValue>({
  containerEl: { current: null },
});

type UseCollapsibleType = (value: boolean) => UseCollapsibleResult;

interface UseCollapsibleResult {
  value: boolean;
  toggle: () => void;
  show: () => void;
  hide: () => void;
}

export const useDataGridCollapsible: UseCollapsibleType = (initialValue) => {
  const [value, setValue] = React.useState(initialValue);

  const toggle = React.useCallback(() => {
    setValue(!value);
  }, [value]);

  const show = React.useCallback(() => {
    setValue(true);
  }, []);

  const hide = React.useCallback(() => {
    setValue(false);
  }, []);

  return { value, toggle, show, hide };
};

export const useScrollShadow = (
  container: React.RefObject<HTMLDivElement | null>
) => {
  return React.useCallback(() => {
    const block = container.current;
    if (!block) return;
    const scroll = block.scrollLeft;
    const clientWidth = block.clientWidth;
    const scrollWidth = block.scrollWidth;
    block.style.setProperty("--scroll-start", scroll > 0 ? "1" : "0");
    block.style.setProperty(
      "--scroll-end",
      scroll + clientWidth !== scrollWidth ? "1" : "0"
    );
  }, []);
};

interface UseFixedOptions {
  classes: Record<string, string>;
  containerEl: React.RefObject<HTMLDivElement | null>;
}

export const useFixed = ({ classes, containerEl }: UseFixedOptions) => {
  const handleScroll = React.useCallback(() => {}, []);

  React.useEffect(() => {
    const container = containerEl.current;
    if (!container) return;
    container.addEventListener("scroll", handleScroll);
    return () => {
      container.removeEventListener("scroll", handleScroll);
    };
  }, []);

  React.useLayoutEffect(() => {
    const container = containerEl.current;
    if (!container) return;
    // @ts-ignore
    const cells: HTMLDivElement[] = container.getElementsByClassName(
      classes.cell
    );
    for (const cell of cells) {
      cell.style.left = "";
      cell.style.top = "";
      cell.style.right = "";
    }
  });

  React.useEffect(() => {
    const container = containerEl.current;
    if (!container) return;
    const rows: HTMLCollectionOf<Element> = container.getElementsByClassName(
      classes.row
    );
    const containerTop = container.offsetTop;
    const containerLeft = container.offsetLeft;
    const containerWidth = container.scrollWidth;

    type CellBatch = {
      cell: HTMLDivElement;
      left?: number;
      top?: number;
      right?: number;
      leftLast?: boolean;
      rightFirst?: boolean;
    };

    const batch: CellBatch[] = [];

    for (const row of rows) {
      const cells: HTMLDivElement[] = [
        ...row.getElementsByClassName(classes.cell),
      ] as HTMLDivElement[];

      // @ts-ignore
      const cellsFixedLeft: HTMLDivElement[] = cells.filter((item) =>
        item.hasAttribute("data-fixed-left")
      );
      const cellsFixedRight: HTMLDivElement[] = cells.filter((item) =>
        item.hasAttribute("data-fixed-right")
      );
      for (const cell of cells) {
        const cellTop = cell.offsetTop;
        const cellLeft = cell.offsetLeft;
        const cellRight = cell.offsetLeft + cell.offsetWidth;

        const current: CellBatch = { cell };

        if (cell.hasAttribute("data-fixed-top")) {
          current.top = cellTop - containerTop;
        }
        if (cell.hasAttribute("data-fixed-left")) {
          current.left = cellLeft - containerLeft;
          current.leftLast = cellsFixedLeft[cellsFixedLeft.length - 1] === cell;
        }
        if (cell.hasAttribute("data-fixed-right")) {
          current.right = containerLeft + containerWidth - cellRight;
          current.rightFirst = cellsFixedRight[0] === cell;
        }

        batch.push(current);
      }
    }

    for (const { cell, left, leftLast, right, rightFirst, top } of batch) {
      if (left !== undefined) {
        cell.style.left = `${left}px`;
      }
      if (right !== undefined) {
        cell.style.right = `${right}px`;
      }
      if (top !== undefined) {
        cell.style.top = `${top}px`;
      }
      if (leftLast) {
        cell.setAttribute("data-fixed-left-last", "");
      }
      if (rightFirst) {
        cell.setAttribute("data-fixed-right-first", "");
      }
    }
  });
};
