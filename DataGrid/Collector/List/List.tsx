import * as React from "react";
import clsx from "clsx";
import {
  Input,
  Checkbox,
  useScrollStyles,
  Typography,
  Button,
} from "@x5-react-uikit/core";

import DataGridCollectorListProps from "./types";
import useStyles from "./styles";
import { values } from "lodash";

export const DataGridCollectorList: React.VFC<DataGridCollectorListProps> = (
  props
) => {
  const classes = useStyles();
  const classesScroll = useScrollStyles();
  const [search, setSearch] = React.useState("");
  const list = React.useMemo(() => {
    let data = props.options || [];
    if (search) {
      data = data.filter((item) => {
        return item.value
          .toLocaleLowerCase()
          .includes(search.toLocaleLowerCase());
      });
    }
    return data;
  }, [props.options, search]);
  const handleChange = React.useCallback(
    (id, value) => {
      if (!props.onChange) return;
      if (value) {
        props.onChange([...props.values, id]);
      } else {
        props.onChange(props.values.filter((item) => item !== id));
      }
    },
    [props.values, props.onChange]
  );
  return (
    <div className={classes.root}>
      <div className={classes.search}>
        <Input
          placeholder={"Поиск"}
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          search
          stretch
        />
      </div>
      <div className={clsx(classes.list, classesScroll.root)}>
        {list?.length > 0 ? (
          list.map((item) => (
            <div key={item.id} className={classes.item}>
              <Checkbox
                label={item.value}
                checked={props.values.includes(item.id)}
                // @ts-ignore
                onChange={(_, value: boolean) => handleChange(item.id, value)}
              />
            </div>
          ))
        ) : (
          <Typography variant={"p2"} className={classes.notFound}>
            Ничего не найдено
          </Typography>
        )}
      </div>
      <div className={classes.action}>
        <Button
          className={classes.button}
          variant={"primary"}
          size={"s"}
          onClick={props?.onSave}
        >
          Сохранить
        </Button>
        <Button
          className={classes.button}
          variant={"outlined"}
          size={"s"}
          onClick={props?.onReset}
        >
          Сбросить
        </Button>
      </div>
    </div>
  );
};
