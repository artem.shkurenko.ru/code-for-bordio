interface DataGridCollectorListProps {
  values: number[];
  options: { id: number; value: string }[];
  onChange?: (values: number[]) => void;
  onSave?: () => void;
  onReset?: () => void;
}

export default DataGridCollectorListProps;
