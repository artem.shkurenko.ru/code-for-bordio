import { makeUiStyles, UITheme } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme: UITheme) => ({
  root: {},
  search: {
    marginBottom: theme.spacing(2),
  },
  list: {
    margin: theme.spacing(0, -2),
  },
  item: {
    padding: theme.spacing(1, 2),
  },
  notFound: {
    margin: theme.spacing(10, 2, 8, 2),
    textAlign: "center",
    color: theme.colors.grey[60],
  },
  action: {
    display: "flex",
    marginTop: theme.spacing(3),
  },
  button: {
    "& + &": {
      marginLeft: theme.spacing(2),
    },
  },
}));

export default useStyles;
