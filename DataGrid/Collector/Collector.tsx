import * as React from "react";
import clsx from "clsx";
import Popover, { PopoverOrigin } from "@material-ui/core/Popover";
import { Typography } from "@x5-react-uikit/core";
import {
  ArrowDown as ArrowDownIcon,
  Filter as FilterIcon,
} from "@x5-react-uikit/icons";

import IconButton from "ui/IconButton";

import useStyles from "./styles";
import type DataGridCollectorProps from "./types";

const anchorOrigin: PopoverOrigin = {
  vertical: "bottom",
  horizontal: "right",
};

const transformOrigin: PopoverOrigin = {
  vertical: "top",
  horizontal: "right",
};

export const DataGridCollector: React.FC<DataGridCollectorProps> = (props) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleClick = React.useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
    },
    []
  );

  const handleClose = React.useCallback(() => {
    setAnchorEl(null);
    if (props.onClose) props.onClose();
  }, [props.onClose]);

  React.useEffect(() => {
    if (!props.handleClose) return;
    props.handleClose(() => () => setAnchorEl(null));
  }, [props.handleClose]);

  const open = Boolean(anchorEl);

  return (
    <>
      <div className={classes.button}>
        <IconButton
          className={clsx({
            [classes.buttonValue]: props.isValue,
          })}
          variant={"ghost"}
          size={"extra-small"}
          onClick={handleClick}
        >
          {props.isValue ? (
            <FilterIcon size={"small"} />
          ) : (
            <ArrowDownIcon size={"small"} />
          )}
        </IconButton>
      </div>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}
        elevation={0}
        classes={{
          paper: classes.root,
        }}
      >
        {props.children}
      </Popover>
    </>
  );
};

export const DataGridCollectorFilter: React.FC = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.block}>
      <Typography variant={"h6"} className={classes.label}>
        Фильтрация
      </Typography>
      <div className={classes.content}>{children}</div>
    </div>
  );
};
