import { makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme) => ({
  button: {
    paddingLeft: theme.spacing(2),
    marginLeft: "auto",
    marginTop: theme.spacing(-1),
    marginRight: theme.spacing(-1),
    marginBottom: theme.spacing(-1),
  },
  buttonValue: {
    color: theme.colors.accent[70],
  },
  root: {
    width: theme.spacing(55),
    boxShadow: `${theme.shadows[20]} !important`,
    padding: theme.spacing(2, 0),
  },
  label: {
    padding: theme.spacing(3),
    color: theme.colors.grey[60],
  },
  content: {
    padding: theme.spacing(0, 3),
  },
}));

export default useStyles;
