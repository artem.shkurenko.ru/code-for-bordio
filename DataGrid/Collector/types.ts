export interface DataGridCollectorProps {
  onClose?: () => void;
  isValue?: boolean;
  handleClose?: (p: () => () => void) => void;
}

export default DataGridCollectorProps;
