import * as React from "react";
import { useHistory } from "react-router-dom";
import clsx from "clsx";

import useStyles from "../styles";

export const DataGridRow: React.FC<{
  link?: string;
}> = ({ children, link }) => {
  const classes = useStyles();
  const history = useHistory();

  const onClick = (e) => {
    if (link) {
      const isLink = e.target.closest("a");
      if (!isLink) {
        history.push(link);
      }
    }
  };

  return (
    <div
      onClick={onClick}
      className={clsx({
        [classes.row]: true,
        [classes.link]: link,
      })}
    >
      {children}
    </div>
  );
};
