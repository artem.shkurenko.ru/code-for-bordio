import * as React from "react";

import useStyles from "./styles";
import DataGridProps from "./types";
import {
  DataGridContext,
  DataGridContextValue,
  useScrollShadow,
  useFixed,
} from "./helpers";

const DataGrid: React.FC<DataGridProps> = ({ children, colWidths }) => {
  const classes = useStyles();
  const containerEl = React.useRef<HTMLDivElement | null>(null);
  const context = React.useMemo<DataGridContextValue>(
    () => ({
      containerEl: containerEl,
    }),
    [containerEl]
  );
  const style = React.useMemo<Record<string, string>>(
    () => ({
      gridTemplateColumns: colWidths.join(" "),
    }),
    [colWidths]
  );

  useFixed({ classes, containerEl });

  const handleScrollShadow = useScrollShadow(containerEl);

  return (
    <DataGridContext.Provider value={context}>
      <div
        className={classes.root}
        onScroll={handleScrollShadow}
        ref={containerEl}
        style={style}
      >
        {children}
      </div>
    </DataGridContext.Provider>
  );
};

export default DataGrid;
