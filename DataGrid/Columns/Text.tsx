import * as React from "react";
import { Typography, makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles(() => ({
  root: {
    overflow: "hidden",
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },
}));

export const DataGridColumnText: React.FC = (props) => {
  const classes = useStyles();
  return <Typography className={classes.root} variant={"p2"} {...props} />;
};
