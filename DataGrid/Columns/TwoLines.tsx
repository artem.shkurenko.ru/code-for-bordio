import * as React from "react";
import { Typography, makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles(() => ({
  root: {
    overflow: "hidden",
    display: "-webkit-box",
    boxOrient: "vertical",
    lineClamp: 2,
  },
}));

export const DataGridColumnTwoLines: React.FC = (props) => {
  const classes = useStyles();
  return <Typography variant={"p2"} className={classes.root} {...props} />;
};
