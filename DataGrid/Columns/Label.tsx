import * as React from "react";
import { makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme) => ({
  root: {
    margin: theme.spacing(-0.5),
  },
}));

export const DataGridColumnLabel: React.FC = ({ children }) => {
  const classes = useStyles();
  return <div className={classes.root}>{children}</div>;
};
