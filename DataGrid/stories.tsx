import * as React from "react";

import { Meta, Story } from "@storybook/react";

import DataGrid, { DataGridHead, DataGridCell, DataGridRow } from "./";

export default {
  title: "UI/DataGrid",
  component: DataGrid,
} as Meta;

export const Default: Story = () => {
  const colWidths = React.useMemo(
    () => [
      "32px",
      "44px",
      "minmax(120px, auto)",
      "minmax(240px, auto)",
      "minmax(180px, auto)",
      "minmax(180px, auto)",
      "minmax(180px, auto)",
      "minmax(180px, auto)",
      "32px",
    ],
    []
  );
  return (
    <div style={{ height: "400px" }}>
      <DataGrid colWidths={colWidths}>
        <DataGridRow>
          <DataGridHead fixedLeft />
          <DataGridHead fixedLeft />
          <DataGridHead fixedLeft>Номер</DataGridHead>
          <DataGridHead>Тип заявки</DataGridHead>
          <DataGridHead>Дата создания</DataGridHead>
          <DataGridHead>Автор заявки</DataGridHead>
          <DataGridHead>Статус</DataGridHead>
          <DataGridHead fixedRight>Ответственный</DataGridHead>
          <DataGridHead fixedRight>1</DataGridHead>
        </DataGridRow>
        {new Array(100).fill(null).map((_, index) => {
          return (
            <DataGridRow key={index}>
              <DataGridCell fixedLeft>-</DataGridCell>
              <DataGridCell fixedLeft>-</DataGridCell>
              <DataGridCell fixedLeft>{index}.2</DataGridCell>
              <DataGridCell>{index}.3</DataGridCell>
              <DataGridCell>{index}.4</DataGridCell>
              <DataGridCell>{index}.5</DataGridCell>
              <DataGridCell>{index}.6</DataGridCell>
              <DataGridCell fixedRight>{index}.7</DataGridCell>
              <DataGridCell fixedRight>-</DataGridCell>
            </DataGridRow>
          );
        })}
      </DataGrid>
    </div>
  );
};

// export const Default: Story = () => {
//   return (
//     <DataGrid border>
//       <DataGridHead>
//         <DataGridHeadRow>
//           <DataGridHeadCell width={250}>Колонка 1</DataGridHeadCell>
//           <DataGridHeadCell width={150}>
//             Колонка 2
//             <DataGridFilter />
//           </DataGridHeadCell>
//           <DataGridHeadCell width={100}>Колонка 3</DataGridHeadCell>
//           <DataGridHeadCell width={100}>Колонка 4</DataGridHeadCell>
//           <DataGridHeadCell width={150}>Колонка 5</DataGridHeadCell>
//           <DataGridHeadCell width={250}>Колонка 6</DataGridHeadCell>
//         </DataGridHeadRow>
//       </DataGridHead>
//       <DataGridBody>
//         <DataGridRow>
//           <DataGridCell>1.1</DataGridCell>
//           <DataGridCell>1.2</DataGridCell>
//           <DataGridCell>1.3</DataGridCell>
//           <DataGridCell>1.4</DataGridCell>
//           <DataGridCell>1.5</DataGridCell>
//           <DataGridCell>1.6</DataGridCell>
//         </DataGridRow>
//       </DataGridBody>
//     </DataGrid>
//   );
// };
//
// export const Hard: Story = () => {
//   const basicCollapsible = useDataGridCollapsible(true);
//   return (
//     <DataGrid border>
//       <DataGridHead>
//         <DataGridHeadRow>
//           <DataGridHeadCell rowSpan={2} width={32} fixedLeft={0} />
//           <DataGridHeadCell rowSpan={2} width={114} fixedLeft={32}>
//             Месяц выхода
//           </DataGridHeadCell>
//           <DataGridHeadCell rowSpan={2} width={68} fixedLeft={146}>
//             ID
//           </DataGridHeadCell>
//           <DataGridHeadCell rowSpan={2} width={166} borderRight fixedLeft={214}>
//             Роль / Компетенция
//           </DataGridHeadCell>
//           <DataGridHeadCell colSpan={basicCollapsible.value ? 8 : 1}>
//             <DataGridCollapse
//               value={basicCollapsible.value}
//               onToggle={basicCollapsible.toggle}
//             />
//             Основная информация
//           </DataGridHeadCell>
//           <DataGridHeadCell rowSpan={2} width={32} borderLeft fixedRight={0} />
//         </DataGridHeadRow>
//         <DataGridHeadRow>
//           <DataGridHeadCell width={220}>Уровень</DataGridHeadCell>
//           {basicCollapsible.value && (
//             <>
//               <DataGridHeadCell width={160}>
//                 Статус потребности
//               </DataGridHeadCell>
//               <DataGridHeadCell width={160}>Подразделение</DataGridHeadCell>
//               <DataGridHeadCell width={120}>Должность</DataGridHeadCell>
//               <DataGridHeadCell width={160}>Домен</DataGridHeadCell>
//               <DataGridHeadCell width={160}>Домен</DataGridHeadCell>
//               <DataGridHeadCell width={160}>ПШР</DataGridHeadCell>
//             </>
//           )}
//         </DataGridHeadRow>
//       </DataGridHead>
//       <DataGridBody>
//         {Array(10)
//           .fill(null)
//           .map((_, index) => {
//             return (
//               <DataGridRow key={index}>
//                 <DataGridCell fixedLeft={0}>-</DataGridCell>
//                 <DataGridCell fixedLeft={32}>{index}.2</DataGridCell>
//                 <DataGridCell fixedLeft={146}>{index}.3</DataGridCell>
//                 <DataGridCell fixedLeft={214} borderRight>
//                   {index}.4
//                 </DataGridCell>
//                 <DataGridCell>{index}.5</DataGridCell>
//                 {basicCollapsible.value && (
//                   <>
//                     <DataGridCell>{index}.6</DataGridCell>
//                     <DataGridCell>{index}.7</DataGridCell>
//                     <DataGridCell>{index}.8</DataGridCell>
//                     <DataGridCell>{index}.9</DataGridCell>
//                     <DataGridCell>{index}.10</DataGridCell>
//                     <DataGridCell>{index}.11</DataGridCell>
//                     <DataGridCell>{index}.12</DataGridCell>
//                   </>
//                 )}
//                 <DataGridCell fixedRight={0} borderLeft>
//                   -
//                 </DataGridCell>
//               </DataGridRow>
//             );
//           })}
//       </DataGridBody>
//     </DataGrid>
//   );
// };
