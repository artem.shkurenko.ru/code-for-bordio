import { makeUiStyles, UITheme } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme: UITheme) => ({
  root: {
    display: "grid",
    overflow: "auto",
    maxHeight: "100%",
    borderRadius: theme.spacing(1),
    width: "100%",
  },
  row: {
    display: "contents",
  },
  link: {
    cursor: "pointer",
  },
  cell: {
    position: "sticky",
    backgroundColor: theme.colors.white,
    zIndex: 2,
    padding: theme.spacing(2),
    borderBottom: `2px solid ${theme.colors.grey[10]}`,
    minHeight: theme.spacing(13),
    boxSizing: "border-box",
    "&[data-border-left]": {
      borderLeft: `1px solid ${theme.colors.grey[10]}`,
    },
    "&[data-border-right]": {
      borderRight: `1px solid ${theme.colors.grey[10]}`,
    },
    "&[data-fixed-left], &[data-fixed-right]": {
      zIndex: 3,
    },
    "&[data-fixed-left-last], &[data-fixed-right-first]": {
      "&:before": {
        content: "''",
        position: "absolute",
        top: 0,
        width: theme.spacing(3),
        height: "100%",
      },
    },
    "&[data-fixed-left-last]:before": {
      left: "100%",
      opacity: "var(--scroll-start, 0)",
      background:
        "linear-gradient(to right, rgba(80, 86, 94, .08), rgba(80, 86, 94, 0))",
    },
    "&[data-fixed-right-first]:before": {
      right: "100%",
      opacity: "var(--scroll-end, 0)",
      background:
        "linear-gradient(to right, rgba(80, 86, 94, 0), rgba(80, 86, 94, .08))",
    },
    "$link:hover &": {
      backgroundColor: theme.colors.grey[10],
    },
  },
  cellWhite: {
    backgroundColor: theme.colors.white,
  },
  cellStroke: {
    background:
      "repeating-linear-gradient(-45deg, #fff, #fff 7px, #EFF1F5 1px, #fff 9px)",
  },
  head: {
    zIndex: 4,
    padding: theme.spacing(3, 2),
    display: "flex",
    alignItems: "flex-end",
    justifyContent: "flex-start",
    minHeight: theme.spacing(10),
    "&[data-fixed-left], &[data-fixed-right]": {
      zIndex: 5,
    },
  },
  headInner: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%",
  },
}));

export default useStyles;
