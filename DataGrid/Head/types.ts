interface DataGridHeadProps {
  fixedLeft?: boolean;
  fixedRight?: boolean;
  colSpan?: number;
  rowSpan?: number;
  borderLeft?: boolean;
  borderRight?: boolean;
}

export interface DataGridHeadTheme {
  colSpan: number;
  rowSpan: number;
  borderLeft: boolean;
  borderRight: boolean;
}

export default DataGridHeadProps;
