import { makeUiStyles } from "@x5-react-uikit/core";

const useStyles = makeUiStyles((theme) => ({
  root: {
    marginRight: theme.spacing(2),
  },
}));

export default useStyles;
