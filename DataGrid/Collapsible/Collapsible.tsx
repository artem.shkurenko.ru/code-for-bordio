import * as React from "react";
import { ColumnExpand, ColumnCollapse } from "@x5-react-uikit/icons";

import IconButton from "ui/IconButton";

import { DataGridColumnButton } from "../";
import DataGridCollapsibleProps from "./types";
import useStyles from "./styles";

export const DataGridCollapsible: React.VFC<DataGridCollapsibleProps> = (
  props
) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <DataGridColumnButton>
        <IconButton
          variant={"ghost"}
          size={"extra-small"}
          onClick={props.onChange}
          data-tooltip={!props.value ? "Раскрыть столбцы" : "Скрыть столбцы"}
        >
          {!props.value ? (
            <ColumnExpand size={"small"} />
          ) : (
            <ColumnCollapse size={"small"} />
          )}
        </IconButton>
      </DataGridColumnButton>
    </div>
  );
};
