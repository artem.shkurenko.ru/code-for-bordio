import * as React from "react";
import clsx from "clsx";
import camelCase from "lodash/camelCase";

import DataGridCellProps, { DataGridCellBackground } from "./types";
import useStyles from "../styles";

export const DataGridCell: React.FC<DataGridCellProps> = ({
  children,
  fixedLeft = false,
  fixedRight = false,
  borderLeft = false,
  borderRight = false,
  background = DataGridCellBackground.WHITE,
}) => {
  const classes = useStyles();
  return (
    <div
      className={clsx(
        classes.cell,
        classes[camelCase(["cell", background].join())]
      )}
      data-fixed-left={fixedLeft ? "" : null}
      data-fixed-right={fixedRight ? "" : null}
      data-border-left={borderLeft ? "" : null}
      data-border-right={borderRight ? "" : null}
    >
      {children}
    </div>
  );
};
