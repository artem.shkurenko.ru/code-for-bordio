interface DataGridCellProps {
  fixedLeft?: boolean;
  fixedRight?: boolean;
  borderLeft?: boolean;
  borderRight?: boolean;
  background?: DataGridCellBackground;
}

export enum DataGridCellBackground {
  WHITE = "WHITE",
  STROKE = "STROKE",
}

export interface DataGridCellTheme {
  borderLeft: boolean;
  borderRight: boolean;
}

export default DataGridCellProps;
